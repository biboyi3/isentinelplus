﻿namespace iSentinel_Project
{
    partial class frmipconfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmipconfig));
            this.lblIpAdd = new System.Windows.Forms.Label();
            this.cmbIPadd = new System.Windows.Forms.ComboBox();
            this.btnok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIpAdd
            // 
            resources.ApplyResources(this.lblIpAdd, "lblIpAdd");
            this.lblIpAdd.Name = "lblIpAdd";
            // 
            // cmbIPadd
            // 
            this.cmbIPadd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIPadd.FormattingEnabled = true;
            resources.ApplyResources(this.cmbIPadd, "cmbIPadd");
            this.cmbIPadd.Name = "cmbIPadd";
            // 
            // btnok
            // 
            resources.ApplyResources(this.btnok, "btnok");
            this.btnok.Name = "btnok";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // frmipconfig
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.cmbIPadd);
            this.Controls.Add(this.lblIpAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmipconfig";
            this.Load += new System.EventHandler(this.frmipconfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIpAdd;
        private System.Windows.Forms.ComboBox cmbIPadd;
        private System.Windows.Forms.Button btnok;
    }
}