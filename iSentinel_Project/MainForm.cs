﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Net;
using Newtonsoft.Json;

namespace iSentinel_Project
{
    public partial class MainForm : Form
    {
        public int ctr = 0;
        public static bool isreadytotap;

        public static Image image;

        private string host = ConfigurationManager.ConnectionStrings["connStringHost"].ConnectionString;
        private string branch = ConfigurationManager.ConnectionStrings["connStringBranch"].ConnectionString;
        

        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "Connect")]
        public static extern IntPtr Connect(string Parameters);

        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "PullLastError")]
        public static extern int PullLastError();

        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "GetRTLog")]
        public static extern int GetRTLog(IntPtr h, ref byte buffer, int buffersize);

        IntPtr h = IntPtr.Zero;

        public static string rfid, type;
        public static string url = "#", dpurl = "#";

        public static int timeTick;
        
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            isreadytotap = true;
            string str = "";
            int ret = 0;

            str = frmipconfig.selectedip;
            
            lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            lblDate.Text = DateTime.Now.ToLongDateString();

            if (IntPtr.Zero == h)
            {
                h = Connect(str);

                if (h != IntPtr.Zero)
                {
                    lblDate.Text = DateTime.Now.ToLongDateString();
                    tmrRTLog.Start();
                }
                else
                {
                    ret = PullLastError();
                    MessageBox.Show("Connect device Failed! The error id is: " + ret);
                }
            }
            
        }

        private void tmrRTLog_Tick(object sender, EventArgs e)
        {
            if (isreadytotap)
            {
                lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

                int ret = 0, buffersize = 256;
                string str = "";
                string[] tmp = null;
                byte[] buffer = new byte[256];

                if (IntPtr.Zero != h)
                {
                    ret = GetRTLog(h, ref buffer[0], buffersize);
                    if (ret >= 0)
                    {
                        str = Encoding.Default.GetString(buffer);
                        tmp = str.Split(',');

                        if (tmp[4] == "27")
                        {
                            isreadytotap = false;
                            InvalidForm invalidForm = new InvalidForm();
                            invalidForm.Show();
                            SetParent(invalidForm.Handle, this.Handle);
                            int heigth;
                            heigth = this.Height - 40;
                            MoveWindow(invalidForm.Handle, 0, 0, this.Width, heigth, true);
                            InvalidForm.timeTick = 0;
                        }
                        if (tmp[4] == "0")
                        {
                            isreadytotap = false;

                            if (tmp[3] == "1") type = "login";
                            else type = "logout";

                            rfid = tmp[2];

                            ValidForm validfrm = new ValidForm();

                            try
                            {
                                // ito ay para ma load ung URL sa User INFORMATION
                                url = "http://" + host + "/" + branch + "/admin/gate.php?statpos=" + type + "&rfid=" + rfid;
                                WebClient wc = new WebClient();
                                var json_String = wc.DownloadString(url);
                                jsonClass data = JsonConvert.DeserializeObject<jsonClass>(json_String);

                                // pag lagay ng value sa mga label ng Valid Form
                                validfrm.lbllastname.Text = data.Lastname;
                                validfrm.lblfirstname.Text = data.Firstname;
                                validfrm.lbllevel.Text = data.YearLevel + " - " + data.SectionName;
                                validfrm.lblmsgboard.Text = data.Message;
                                validfrm.lbltime.Text = data.AddWhen;
                                validfrm.lbldate.Text = DateTime.Now.ToString("hh:mm:ss tt");
                                validfrm.lblday.Text = System.DateTime.Now.DayOfWeek.ToString();

                                // ito ay para ma load ung URL sa User IMAGE
                                dpurl = "http://" + host + "/" + branch + "/admin/gate.php?img2=" + data.StudentId;
                                var jsonimage_String = wc.DownloadString(dpurl);
                                jsonImageClass imgdata = JsonConvert.DeserializeObject<jsonImageClass>(jsonimage_String);
                                validfrm.setImage(Base64ToImage(imgdata.DisplayPhoto));

                                // ididisplay depende kung papasok ba o palabas ung user
                                if (type == "login")
                                {
                                    validfrm.lblstatbar.BackColor = Color.ForestGreen;
                                    validfrm.lblwelcome.BackColor = Color.ForestGreen;
                                    validfrm.lbltime.BackColor = Color.ForestGreen;
                                    validfrm.lbldate.BackColor = Color.ForestGreen;
                                    validfrm.lblday.BackColor = Color.ForestGreen;
                                    validfrm.lblwelcome.Text = "Welcome!";
                                }
                                else
                                {
                                    validfrm.lblstatbar.BackColor = Color.Red;
                                    validfrm.lblwelcome.BackColor = Color.Red;
                                    validfrm.lbltime.BackColor = Color.Red;
                                    validfrm.lbldate.BackColor = Color.Red;
                                    validfrm.lblday.BackColor = Color.Red;
                                    validfrm.lblwelcome.Text = "Thank you!";
                                }

                                validfrm.Show();
                                SetParent(validfrm.Handle, this.Handle);
                                int heigth;
                                heigth = this.Height - 40;
                                MoveWindow(validfrm.Handle, 0, 0, this.Width, heigth, true);
                                ValidForm.timeTick = 0;
                            }
                            catch
                            {
                                MessageBox.Show("Data can't load. Please get the information of the ID.");
                            }
                        }
                    }
                }
                else
                {
                    //MessageBox.Show("Connect device Failed! The error id is: " + ret);
                    return;
                }
            }
        }

        private void tableLayoutPanel1_Resize(object sender, EventArgs e)
        {
            try
            {
                if (lblTime.Width / 8 > 0)
                {
                    lblTime.Font = new Font(lblTime.Font.FontFamily, lblTime.Width / 8, lblTime.Font.Style, GraphicsUnit.Point);
                }

                if (lblPacoCathSchl.Width / 16 > 0)
                {
                    lblPacoCathSchl.Font = new Font(lblPacoCathSchl.Font.FontFamily, lblPacoCathSchl.Width / 16, lblPacoCathSchl.Font.Style, GraphicsUnit.Point);
                }

                if (lblDate.Width / 24 > 0)
                {
                    lblDate.Font = new Font(lblDate.Font.FontFamily, lblDate.Width / 24, lblDate.Font.Style, GraphicsUnit.Point);
                }

                if (pictureBox1.Width / 24 > 0)
                {
                    this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width/24, pictureBox1.Height/24);
                    this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
                    this.pictureBox1.TabIndex = 6;
                }
                
            }
            catch
            {

            }
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);
        
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
            this.Close();
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);

            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                return Image.FromStream(ms, true);
                
            }
        }
    }
}
