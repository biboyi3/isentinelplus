﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSentinel_Project
{
    public partial class ValidForm : Form
    {
        public static int timeTick;

        public ValidForm()
        {
            InitializeComponent();
        }

        private void ValidForm_Load(object sender, EventArgs e)
        {
            tmrShowForm.Start();
        }

        public void setImage(Image img)
        {
            pctrboxperson.Image = img;
        }

        private void tmrShowForm_Tick(object sender, EventArgs e)
        {
            timeTick += 1;
            if (timeTick >= 1)
            {
                tmrShowForm.Stop();
                this.Close();
            }
        }
        
        private void ValidForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            MainForm.isreadytotap = true;
        }

    }
}
