﻿namespace iSentinel_Project
{
    partial class InvalidForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvalidForm));
            this.lblTimeDate = new System.Windows.Forms.Label();
            this.tmrShowForm = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTimeDate
            // 
            this.lblTimeDate.BackColor = System.Drawing.Color.White;
            this.lblTimeDate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTimeDate.Font = new System.Drawing.Font("Digital-7", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeDate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTimeDate.Location = new System.Drawing.Point(0, 672);
            this.lblTimeDate.Name = "lblTimeDate";
            this.lblTimeDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTimeDate.Size = new System.Drawing.Size(1457, 68);
            this.lblTimeDate.TabIndex = 0;
            this.lblTimeDate.Text = "lblTimeDate";
            // 
            // tmrShowForm
            // 
            this.tmrShowForm.Interval = 1000;
            this.tmrShowForm.Tick += new System.EventHandler(this.tmrShowForm_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80.24999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(240, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1025, 179);
            this.label1.TabIndex = 1;
            this.label1.Text = "UNRECOGNIZED";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80.24999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(240, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1033, 191);
            this.label2.TabIndex = 2;
            this.label2.Text = "IDENTIFICATION";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InvalidForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(1457, 740);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTimeDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InvalidForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iSentinel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.InvalidForm_FormClosed);
            this.Load += new System.EventHandler(this.InvalidForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTimeDate;
        private System.Windows.Forms.Timer tmrShowForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}