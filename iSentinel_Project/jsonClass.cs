﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSentinel_Project
{
    class jsonClass
    {

        [JsonProperty("stud_applicant_id")]
        public string StudentId { get; set; }

        [JsonProperty("addwhen")]
        public string AddWhen { get; set; } //e2

        [JsonProperty("ppd_lastname")]
        public string Lastname { get; set; } //e2

        [JsonProperty("ppd_firstname")]
        public string Firstname { get; set; } //e2

        [JsonProperty("yl_name")]
        public string YearLevel { get; set; } //e2

        [JsonProperty("section_name")]
        public string SectionName { get; set; } //e2

        [JsonProperty("message_")]
        public string Message { get; set; }
        
        
        //http://192.168.5.115/isentinel/admin/gate.php?img2=7732566
    }
}
