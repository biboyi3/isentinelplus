﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace iSentinel_Project
{
    public partial class frmipconfig : Form
    {
        private string protocol = ConfigurationManager.ConnectionStrings["connString_protocol"].ConnectionString;
        private string port = ConfigurationManager.ConnectionStrings["connString_port"].ConnectionString;
        private string timeout = ConfigurationManager.ConnectionStrings["connString_timeout"].ConnectionString;
        private string password = ConfigurationManager.ConnectionStrings["connString_password"].ConnectionString;

        private static string ips = ConfigurationManager.ConnectionStrings["connString_ip"].ConnectionString;
        string[] ipNumbers = ips.Split(',');

        public static string selectedip;
        public static int selectedipadd;

        public frmipconfig()
        {
            InitializeComponent();
        }

        private void frmipconfig_Load(object sender, EventArgs e)
        {
            foreach (string x in ipNumbers)
            {
                cmbIPadd.Items.Add(x);
            }
            cmbIPadd.SelectedIndex = 0;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            selectedip = "protocol=" + protocol + ",ipaddress=" + cmbIPadd.SelectedItem.ToString() + ",port=" + port + ",timeout=" + timeout + ",passwd=" + password;
            
            selectedipadd = cmbIPadd.SelectedIndex;

            MainForm mainform = new MainForm();
            mainform.FormClosed += new FormClosedEventHandler(mainform_FormClosed);
            mainform.Show();
            this.Hide();
        }

        private void mainform_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
