﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSentinel_Project
{
    public partial class InvalidForm : Form
    {
        public static int timeTick;

        public InvalidForm()
        {
            InitializeComponent();
        }

        private void InvalidForm_Load(object sender, EventArgs e)
        {
            lblTimeDate.Text = DateTime.Now.ToString("hh:mm:ss tt MM/dd/yyyy");
            tmrShowForm.Start();
        }

        private void tmrShowForm_Tick(object sender, EventArgs e)
        {
            lblTimeDate.Text = DateTime.Now.ToString("tt hh:mm:ss MM/dd/yyyy");
            timeTick += 1;
            if (timeTick >= 1)
            {
                tmrShowForm.Stop();
                this.Close();
            }
        }

        private void InvalidForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            MainForm.isreadytotap = true;
        }


    }
}
