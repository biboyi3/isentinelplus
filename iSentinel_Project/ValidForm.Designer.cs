﻿namespace iSentinel_Project
{
    partial class ValidForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidForm));
            this.tmrShowForm = new System.Windows.Forms.Timer(this.components);
            this.lblPacoCathSchl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblwelcome = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.lblday = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblmsgboard = new System.Windows.Forms.Label();
            this.lblboard = new System.Windows.Forms.Label();
            this.lbllevel = new System.Windows.Forms.Label();
            this.lbldepartment = new System.Windows.Forms.Label();
            this.lblfirstname = new System.Windows.Forms.Label();
            this.lbllastname = new System.Windows.Forms.Label();
            this.pctrboxperson = new System.Windows.Forms.PictureBox();
            this.lblstatbar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrboxperson)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrShowForm
            // 
            this.tmrShowForm.Interval = 1000;
            this.tmrShowForm.Tick += new System.EventHandler(this.tmrShowForm_Tick);
            // 
            // lblPacoCathSchl
            // 
            this.lblPacoCathSchl.AutoSize = true;
            this.lblPacoCathSchl.BackColor = System.Drawing.Color.White;
            this.lblPacoCathSchl.Font = new System.Drawing.Font("Titillium Web", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacoCathSchl.ForeColor = System.Drawing.Color.DimGray;
            this.lblPacoCathSchl.Location = new System.Drawing.Point(1, 18);
            this.lblPacoCathSchl.Margin = new System.Windows.Forms.Padding(0);
            this.lblPacoCathSchl.Name = "lblPacoCathSchl";
            this.lblPacoCathSchl.Size = new System.Drawing.Size(540, 73);
            this.lblPacoCathSchl.TabIndex = 6;
            this.lblPacoCathSchl.Text = "PACO CATHOLIC SCHOOL";
            this.lblPacoCathSchl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(-5, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(930, 11);
            this.label1.TabIndex = 8;
            // 
            // lblwelcome
            // 
            this.lblwelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblwelcome.AutoSize = true;
            this.lblwelcome.BackColor = System.Drawing.Color.ForestGreen;
            this.lblwelcome.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwelcome.ForeColor = System.Drawing.Color.White;
            this.lblwelcome.Location = new System.Drawing.Point(0, 772);
            this.lblwelcome.Name = "lblwelcome";
            this.lblwelcome.Size = new System.Drawing.Size(158, 57);
            this.lblwelcome.TabIndex = 9;
            this.lblwelcome.Text = "Status!";
            this.lblwelcome.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbltime
            // 
            this.lbltime.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.ForestGreen;
            this.lbltime.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Location = new System.Drawing.Point(234, 774);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(113, 57);
            this.lbltime.TabIndex = 10;
            this.lbltime.Text = "Time";
            this.lbltime.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbldate
            // 
            this.lbldate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbldate.AutoSize = true;
            this.lbldate.BackColor = System.Drawing.Color.ForestGreen;
            this.lbldate.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldate.ForeColor = System.Drawing.Color.White;
            this.lbldate.Location = new System.Drawing.Point(667, 792);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(65, 33);
            this.lbldate.TabIndex = 12;
            this.lbldate.Text = "Date";
            this.lbldate.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblday
            // 
            this.lblday.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblday.AutoSize = true;
            this.lblday.BackColor = System.Drawing.Color.ForestGreen;
            this.lblday.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblday.ForeColor = System.Drawing.Color.White;
            this.lblday.Location = new System.Drawing.Point(667, 764);
            this.lblday.Name = "lblday";
            this.lblday.Size = new System.Drawing.Size(56, 33);
            this.lblday.TabIndex = 13;
            this.lblday.Text = "Day";
            this.lblday.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::iSentinel_Project.Properties.Resources.elogo2_;
            this.pictureBox1.Location = new System.Drawing.Point(610, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.lblmsgboard);
            this.panel1.Controls.Add(this.lblboard);
            this.panel1.Controls.Add(this.lbllevel);
            this.panel1.Controls.Add(this.lbldepartment);
            this.panel1.Controls.Add(this.lblfirstname);
            this.panel1.Controls.Add(this.lbllastname);
            this.panel1.Controls.Add(this.pctrboxperson);
            this.panel1.Location = new System.Drawing.Point(-2, 102);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(918, 667);
            this.panel1.TabIndex = 14;
            // 
            // lblmsgboard
            // 
            this.lblmsgboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblmsgboard.BackColor = System.Drawing.Color.LightGray;
            this.lblmsgboard.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsgboard.ForeColor = System.Drawing.Color.Black;
            this.lblmsgboard.Location = new System.Drawing.Point(11, 427);
            this.lblmsgboard.Name = "lblmsgboard";
            this.lblmsgboard.Size = new System.Drawing.Size(483, 235);
            this.lblmsgboard.TabIndex = 21;
            // 
            // lblboard
            // 
            this.lblboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblboard.AutoSize = true;
            this.lblboard.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblboard.ForeColor = System.Drawing.Color.Gray;
            this.lblboard.Location = new System.Drawing.Point(12, 389);
            this.lblboard.Name = "lblboard";
            this.lblboard.Size = new System.Drawing.Size(125, 23);
            this.lblboard.TabIndex = 20;
            this.lblboard.Text = "Message Board:";
            // 
            // lbllevel
            // 
            this.lbllevel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbllevel.AutoSize = true;
            this.lbllevel.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllevel.Location = new System.Drawing.Point(2, 300);
            this.lbllevel.Name = "lbllevel";
            this.lbllevel.Size = new System.Drawing.Size(241, 57);
            this.lbllevel.TabIndex = 19;
            this.lbllevel.Text = "Grade Level";
            // 
            // lbldepartment
            // 
            this.lbldepartment.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbldepartment.AutoSize = true;
            this.lbldepartment.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldepartment.ForeColor = System.Drawing.Color.Gray;
            this.lbldepartment.Location = new System.Drawing.Point(11, 277);
            this.lbldepartment.Name = "lbldepartment";
            this.lbldepartment.Size = new System.Drawing.Size(94, 23);
            this.lbldepartment.TabIndex = 18;
            this.lbldepartment.Text = "Department:";
            // 
            // lblfirstname
            // 
            this.lblfirstname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblfirstname.AutoSize = true;
            this.lblfirstname.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfirstname.Location = new System.Drawing.Point(3, 176);
            this.lblfirstname.Name = "lblfirstname";
            this.lblfirstname.Size = new System.Drawing.Size(201, 57);
            this.lblfirstname.TabIndex = 17;
            this.lblfirstname.Text = "Firstname";
            // 
            // lbllastname
            // 
            this.lbllastname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbllastname.AutoSize = true;
            this.lbllastname.Font = new System.Drawing.Font("Arial Narrow", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllastname.Location = new System.Drawing.Point(-3, 82);
            this.lbllastname.Name = "lbllastname";
            this.lbllastname.Size = new System.Drawing.Size(291, 75);
            this.lbllastname.TabIndex = 16;
            this.lbllastname.Text = "Lastname,";
            // 
            // pctrboxperson
            // 
            this.pctrboxperson.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pctrboxperson.Image = global::iSentinel_Project.Properties.Resources.user;
            this.pctrboxperson.Location = new System.Drawing.Point(492, 20);
            this.pctrboxperson.Name = "pctrboxperson";
            this.pctrboxperson.Size = new System.Drawing.Size(404, 268);
            this.pctrboxperson.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctrboxperson.TabIndex = 15;
            this.pctrboxperson.TabStop = false;
            // 
            // lblstatbar
            // 
            this.lblstatbar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblstatbar.BackColor = System.Drawing.Color.ForestGreen;
            this.lblstatbar.Location = new System.Drawing.Point(-5, 764);
            this.lblstatbar.Name = "lblstatbar";
            this.lblstatbar.Size = new System.Drawing.Size(930, 91);
            this.lblstatbar.TabIndex = 15;
            // 
            // ValidForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(918, 854);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblday);
            this.Controls.Add(this.lbldate);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.lblwelcome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblPacoCathSchl);
            this.Controls.Add(this.lblstatbar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ValidForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iSentinel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ValidForm_FormClosed);
            this.Load += new System.EventHandler(this.ValidForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrboxperson)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Timer tmrShowForm;
        public System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.Label lblPacoCathSchl;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblwelcome;
        public System.Windows.Forms.Label lbltime;
        public System.Windows.Forms.Label lbldate;
        public System.Windows.Forms.Label lblday;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblboard;
        public System.Windows.Forms.Label lbllevel;
        public System.Windows.Forms.Label lbldepartment;
        public System.Windows.Forms.Label lblfirstname;
        public System.Windows.Forms.Label lbllastname;
        public System.Windows.Forms.PictureBox pctrboxperson;
        public System.Windows.Forms.Label lblmsgboard;
        public System.Windows.Forms.Label lblstatbar;
    }
}